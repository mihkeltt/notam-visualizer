﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Web.Notam;

namespace Api.Services
{
	public class NotamService
	{
		public const string Lower = "LOWER:";
		public const string Upper = "UPPER:";
		public const string From = "FROM:";
		public const string To = "TO:";
		public const string Schedule = "SCHEDULE:";

		public readonly List<string> Labels = new List<string> { Lower, Upper, From, To, Schedule };

		public List<Area> GetAllAreas(string notamPibUrl, DateTime from, DateTime to)
		{
			string sourceText;
			using (var webClient = new WebClient())
			{
				var pdfData = webClient.DownloadData(notamPibUrl);

				using (var pdfReader = new PdfReader(pdfData))
				{
					var sb = new StringBuilder();

					for (var i = 1; i <= pdfReader.NumberOfPages; i++)
					{
						sb.Append(PdfTextExtractor.GetTextFromPage(pdfReader, i));
					}

					sourceText = sb.ToString();
				}
			}

			var startIndex = sourceText.IndexOf("NAV WARNINGS") + 12;
			var endIndex = sourceText.IndexOf("PERMANENT AND LONG ");
			var navWarningsText = sourceText.Substring(startIndex, endIndex - startIndex);

			var newNotams = new List<Area>();

			var predefinedAreas = AeronauticalAreas.PredefinedAreas;
			var notamsTexts = navWarningsText.Split(new string[] { "Show in map" }, StringSplitOptions.RemoveEmptyEntries)
				.Select(t => t.Trim(' ', '+', '\n'))
				.Where(t => !String.IsNullOrWhiteSpace(t))
				.Select(t => t.Replace("\n", "\r\n").ToUpper())
				.ToList();

			foreach (var t in notamsTexts)
			{
				var description = GetNotamDescription(t);
				var labelTexts = GetLabeledValues(t);

				var notam = new Area
				{
					OriginalText = t,
					Description = description
				};

				var predefinedArea = predefinedAreas
					.FirstOrDefault(pa =>
					{
						var upperText = description.ToUpper();

						return (upperText.Contains(" ACTIVATED.") || upperText.Contains(" ACT.")) && upperText.Contains(pa.FullName.ToUpper());
					});

				if (predefinedArea != null)
				{
					notam = predefinedArea;
					notam.ActiveOverride = null;
				}
				else
					newNotams.Add(notam);

				if (predefinedArea == null)
				{
					var pointMatches = Regex.Matches(t, Point.NECoordPattern);

					if (pointMatches.Count == 1)
					{
						notam.Location = new Circle
						{
							CenterPoint = Point.FromNEString(pointMatches[0].Value)
						};
					}
					else if (pointMatches.Count > 1)
					{
						notam.Location = new Polygon
						{
							Points = pointMatches
								.Select(pm => Point.FromNEString(pm.Value))
								.ToList()
						};
					}

					if (t.Contains("TEMPORARY RESTRICTED AREA"))
						notam.Type = AreaType.TemporaryRestrictedArea;
					else if (t.Contains("RESTRICTED AREA"))
						notam.Type = AreaType.RestrictedArea;
					else if (t.Contains("DANGER AREA"))
						notam.Type = AreaType.DangerArea;
				}

				foreach (var labelText in labelTexts)
				{
					if (labelText.Key == Lower)
					{
						notam.Lower = labelText.Value;
					}
					else if (labelText.Key == Upper)
					{
						notam.Upper = labelText.Value;
					}
					else if (labelText.Key == From || labelText.Key == To)
					{
						var dateTimeRegex = new Regex(@"(?<day>\d{2}) (?<month>\w{3}) (?<year>\d{4}) (?<hour>\d{2}):(?<minute>\d{2})");
						var match = dateTimeRegex.Match(labelText.Value);
						if (!match.Success)
							continue;

						var date = DateTime.ParseExact(
							match.Value,
							"dd MMM yyyy HH:mm",
							CultureInfo.InvariantCulture
							);

						if (labelText.Key == From)
							notam.From = date;
						else if (labelText.Key == To)
							notam.To = date;
					}
					else if (labelText.Key == Schedule)
					{
						var scheduleString = labelText.Value;

						var schedule = Web.Notam.Schedule.FromString(scheduleString, notam.From.Year, notam.From.Month);

						notam.Schedule = schedule;
					}
				}
			}

			var allAreas = predefinedAreas.Union(newNotams).ToList();

			allAreas = allAreas
				.Where(a => a.IsActive(from, to))
				.ToList();

			return allAreas;
		}

		private Dictionary<string, string> GetLabeledValues(string notamText)
		{
			var labelPositions = GetLabelPositions(notamText);

			if (labelPositions.Count == 0)
				return new Dictionary<string, string>();

			var orderedPositions = labelPositions
				.OrderBy(kvp => kvp.Position)
				.ToList();

			var result = new Dictionary<string, string>();

			for (var i = 0; i < orderedPositions.Count; i++)
			{
				var isLast = i == orderedPositions.Count - 1;
				var labelPosition = orderedPositions[i];
				var nextLabelPosition = isLast ? null : orderedPositions[i + 1];

				var startIndex = labelPosition.Position + labelPosition.Label.Length;
				var endIndex = (nextLabelPosition?.Position).GetValueOrDefault(notamText.Length);
				var labelText = notamText.Substring(startIndex, endIndex - startIndex).Replace("\r\n", " ").Trim();

				result[labelPosition.Label] = labelText;
			}

			return result;
		}

		private string GetNotamDescription(string notamText)
		{
			var labelPositions = GetLabelPositions(notamText);

			if (labelPositions.Count == 0)
				return notamText.Trim();

			var firstLabel = labelPositions.OrderBy(lp => lp.Position).First();

			return notamText.Substring(0, firstLabel.Position).Trim();
		}

		private List<LabelPosition> GetLabelPositions(string notamText)
		{
			return Labels
				.Select(l => new LabelPosition { Label = l, Position = notamText.IndexOf(l) })
				.Where(l => l.Position >= 0)
				.ToList();
		}

		private class LabelPosition
		{
			public string Label { get; set; }
			public int Position { get; set; }
		}
	}
}