﻿using System;

namespace Web.Models.Notam
{
	public class AreaRequest
	{
		public DateTime? FromUtc { get; set; }

		public DateTime? ToUtc { get; set; }
	}
}