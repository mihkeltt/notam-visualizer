﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Web.Models.Notam;
using Web.Notam;

namespace Web.Controllers
{
	public class NotamController : Controller
	{
		public IConfiguration Configuration { get; }
		public NotamService NotamService { get; }

		public NotamController(IConfiguration configuration, NotamService notamService)
		{
			Configuration = configuration;
			NotamService = notamService;
		}

		public ActionResult Index()
		{
			var pdfUrl = Configuration["NotamPibLocation"];

			return Json(new
			{
				PdfUrl = pdfUrl
			});
		}

		[HttpPost]
		public ActionResult GetAreas([FromBody]AreaRequest request)
		{
			var from = request.FromUtc ?? DateTime.MinValue;
			var to = request.ToUtc ?? DateTime.MaxValue;
			var allAreas = NotamService.GetAllAreas(Configuration["NotamPibLocation"], from, to);

			return Json(new { Areas = allAreas });
		}
	}
}
