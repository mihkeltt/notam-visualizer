﻿using Api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services
				.AddMvc()
				.AddJsonOptions(options =>
					{
						options.SerializerSettings.Converters.Add(new StringEnumConverter());
						options.SerializerSettings.NullValueHandling = NullValueHandling.Include;
						options.SerializerSettings.TypeNameHandling = TypeNameHandling.Auto;
						options.SerializerSettings.Formatting = Formatting.Indented;
					});
			services.AddSingleton<IConfiguration>(Configuration);
			services.AddSingleton<NotamService>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseDefaultFiles();
			app.UseStaticFiles();
			app.UseMvc(routes =>
			{
				routes.MapRoute("api-rpc", "api/{controller=home}/{action=index}/{id?}");
			});
		}
	}
}
