﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Web.Notam
{
	public class Schedule
	{
		public Schedule()
		{
			Entries = new List<ScheduleEntry>();
		}

		public List<ScheduleEntry> Entries { get; set; }

		public bool HasEntries => Entries?.Count > 0;

		public static Schedule FromString(string scheduleString, int year, int month)
		{
			scheduleString = Regex.Replace(scheduleString, @"\s+-\s+", "-");
			var parts = scheduleString.Split(",");
			var regex1 = new Regex(@"(?<startDay>\d{2}) (?<startHour>\d{2})(?<startMinute>\d{2})-(?<endDay>\d{2}) (?<endHour>\d{2})(?<endMinute>\d{2})");
			var regex2 = new Regex(@"(?<startDay>\d{2})-(?<endDay>\d{2}) (?<startHour>\d{2})(?<startMinute>\d{2})-(?<endHour>\d{2})(?<endMinute>\d{2})");

			var entries = new List<ScheduleEntry>();
			int? startHour;
			int? startMinute;
			int? endHour;
			int? endMinute;

			foreach (var part in parts)
			{
				// DD HHMM-DD HHMM
				var match1 = regex1.Match(part);
				if (match1.Success)
				{
					entries.Add(new ScheduleEntry
					{
						From = ParseScheduleDate1(year, month, match1.Groups["startDay"].Value, match1.Groups["startHour"].Value, match1.Groups["startMinute"].Value),
						To = ParseScheduleDate1(year, month, match1.Groups["endDay"].Value, match1.Groups["endHour"].Value, match1.Groups["endMinute"].Value)
					});

					continue;
				}

				// DD-DD HHMM-HHMM
				var match2 = regex2.Match(part);
				if (match2.Success)
				{
					var startDay = Int32.Parse(match2.Groups["startDay"].Value);
					var endDay = Int32.Parse(match2.Groups["endDay"].Value);
					startHour = Int32.Parse(match2.Groups["startHour"].Value);
					startMinute = Int32.Parse(match2.Groups["startMinute"].Value);
					endHour = Int32.Parse(match2.Groups["endHour"].Value);
					endMinute = Int32.Parse(match2.Groups["endMinute"].Value);

					for (var day = startDay; day <= endDay; day++)
					{
						entries.Add(new ScheduleEntry
						{
							From = new DateTime(year, month, day, startHour.Value, startMinute.Value, 0),
							To = new DateTime(year, month, day, endHour.Value, endMinute.Value, 0)
						});
					}

					continue;
				}

				// DD DD-DD DD... HHMM-HHMM
				var subParts = part.Split(" ", StringSplitOptions.RemoveEmptyEntries);
				var days = new List<int>();
				startHour = startMinute = endHour = endMinute = null;

				foreach (var subPart in subParts)
				{
					if (Regex.IsMatch(subPart, @"^\d{2}$"))
					{
						days.Add(Int32.Parse(subPart));
					}
					else if (Regex.IsMatch(subPart, @"^\d{2}-\d{2}$"))
					{
						var startDay = Int32.Parse(subPart.Substring(0, 2));
						var endDay = Int32.Parse(subPart.Substring(3, 2));

						for (var day = startDay; day <= endDay; day++)
						{
							days.Add(day);
						}
					}
					else if (Regex.IsMatch(subPart, @"^\d{4}-\d{4}$"))
					{
						startHour = Int32.Parse(subPart.Substring(0, 2));
						startMinute = Int32.Parse(subPart.Substring(2, 2));
						endHour = Int32.Parse(subPart.Substring(5, 2));
						endMinute = Int32.Parse(subPart.Substring(7, 2));
					}
				}

				if (new List<int?> { startHour, startMinute, endHour, endMinute }.Any(x => x == null))
					continue;

				foreach (var day in days)
				{
					entries.Add(new ScheduleEntry
					{
						From = new DateTime(year, month, day, startHour.Value, startMinute.Value, 0),
						To = new DateTime(year, month, day, endHour.Value, endMinute.Value, 0)
					});
				}
			}

			return new Schedule
			{
				Entries = entries
			};
		}

		private static DateTime ParseScheduleDate1(int year, int month, string day, string hour, string minute)
		{
			return new DateTime(
				year,
				month,
				Int32.Parse(day),
				Int32.Parse(hour),
				Int32.Parse(minute),
				0
			);
		}
	}
}