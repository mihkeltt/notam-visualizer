﻿using System;
using System.Linq;

namespace Web.Notam
{
	public class Area
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public Shape Location { get; set; }
		public Shape ExcludeLocation { get; set; }
		public string Lower { get; set; }
		public string Upper { get; set; }
		public string OriginalText { get; set; }
		public string NotamDescription { get; set; }
		public string DesignatedName { get; set; }
		public AreaType Type { get; set; }
		public DateTime From { get; set; } = DateTime.MinValue;
		public DateTime To { get; set; } = DateTime.MaxValue;
		public Schedule Schedule { get; set; }
		public bool? ActiveOverride { get; set; }

		public Area()
		{
		}

		public Area(AreaType type, string id, string name, string lower, string upper, Shape location, Shape excludeLocation, string description, bool? activeOverride = null)
		{
			Type = type;
			Location = location;
			ExcludeLocation = excludeLocation;
			Id = id;
			Name = name;
			Lower = lower;
			Upper = upper;
			Description = description;
			ActiveOverride = activeOverride;
		}

		public string FullName => $"{Id} {Name}".Trim();

		public virtual string FullDescription => FullName + "\r\n" + Description;

		public bool IsWholeDay => Schedule == null || Schedule.Entries == null || Schedule.Entries.Count == 0;

		public bool IsActive(DateTime fromUtc, DateTime toUtc)
		{
			if (ActiveOverride.HasValue)
				return ActiveOverride.Value;

			if (IsWholeDay)
				return From <= toUtc && fromUtc <= To;

			return Schedule.Entries.Any(s => s.From <= toUtc && fromUtc <= s.To);
		}
	}
}