﻿namespace Web.Notam
{
	public class PointShape : Shape
	{
		public Point CenterPoint { get; set; }
	}
}