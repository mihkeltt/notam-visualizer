﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Web.Notam
{
	[JsonConverter(typeof(StringEnumConverter))]
	public enum AreaType
	{
		[EnumMember(Value = "TemporaryRestrictedArea")]
		TemporaryRestrictedArea = 1,
		[EnumMember(Value = "RestrictedArea")]
		RestrictedArea = 2,
		[EnumMember(Value = "DangerArea")]
		DangerArea = 3,
		[EnumMember(Value = "ExerciseAndTraining")]
		ExerciseAndTraining = 4,
		[EnumMember(Value = "Paramotoring")]
		Paramotoring = 5,
		[EnumMember(Value = "ParachuteJumping")]
		ParachuteJumping = 6,
		[EnumMember(Value = "Gliding")]
		Gliding = 7,
		[EnumMember(Value = "UnmannedAircraft")]
		UnmannedAircraft = 8,
		[EnumMember(Value = "Aerodrome")]
		Aerodrome = 9
	}
}