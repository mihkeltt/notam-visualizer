﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Web.Notam
{
	public class Polygon : Shape
	{
		public List<Point> Points { get; set; }

		public static Polygon Empty()
		{
			return new Polygon
			{
				Points = new List<Point>()
			};
		}

		public static Polygon FromPointsString(string points)
		{
			if (String.IsNullOrWhiteSpace(points))
				return Polygon.Empty();

			var matches = Regex.Matches(points, Point.NECoordPattern);

			if (matches.Count == 0)
				return Polygon.Empty();

			return new Polygon
			{
				Points = matches
					.Select(m => Point.FromNEString(m.Value))
					.ToList()
			};
		}

		public static Polygon FromKmlString(string points)
		{
			if (String.IsNullOrWhiteSpace(points))
				return Polygon.Empty();

			var matches = Regex.Matches(points, Point.KmlCoordPattern);

			if (matches.Count == 0)
				return Polygon.Empty();

			return new Polygon
			{
				Points = matches
					.Select(m => Point.FromKmlString(m.Value))
					.ToList()
			};
		}

		public override string ToString()
		{
			if (Points == null)
				return "";

			return String.Join(" - ", Points.Select(p => p.ToString()));
		}
	}
}