﻿using System;

namespace Web.Notam
{
	public class ScheduleEntry
	{
		public DateTime From { get; set; }
		public DateTime To { get; set; }
	}
}