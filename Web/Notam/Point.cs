﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using DotSpatial.Projections;

namespace Web.Notam
{
	public class Point
	{
		public const string NECoordPattern = @"(\d{6})N\s+(\d{7})E";
		public const string KmlCoordPattern = @"((?<lng>-?\d+\.\d+),(?<lat>-?\d+\.\d+)(,-?\d+(\.\d+)?)?)";

		public double Latitude { get; }
		public double Longitude { get; }

		public Point(double latitude, double longitude)
		{
			Latitude = latitude;
			Longitude = longitude;
		}

		/// <summary>
		/// Creates a Google Maps compatible point (EPSG:3857 WGS 84 / Pseudo-Mercator) from e-AIP coordinate system string (EPSG:42310 WGS84+GRS80 / Mercator).
		/// </summary>
		public static Point FromNEString(string eAipNeString)
		{
			if (String.IsNullOrWhiteSpace(eAipNeString))
				throw new ArgumentException("No value provided for coordinate string.", nameof(eAipNeString));

			var finalString = eAipNeString.Trim();

			var match = Regex.Match(finalString, NECoordPattern);
			if (!match.Success)
				throw new ArgumentException("Invalid format of coordinate string.", nameof(eAipNeString));

			var latS = match.Groups[1].Value;
			var lngS = match.Groups[2].Value;

			var latitude = Double.Parse(latS.Substring(0, 2)) + (Double.Parse(latS.Substring(2, 2)) / 60) + (Double.Parse(latS.Substring(4, 2)) / 3600);
			var longitude = Double.Parse(lngS.Substring(0, 3)) + (Double.Parse(lngS.Substring(3, 2)) / 60) + (Double.Parse(lngS.Substring(5, 2)) / 3600);

			return new Point(latitude, longitude);
		}

		/// <summary>
		/// Creates a Google Maps compatible point (EPSG:3857 WGS 84 / Pseudo-Mercator) from KML coordinate system string.
		/// </summary>
		public static Point FromKmlString(string kmlString)
		{
			if (String.IsNullOrWhiteSpace(kmlString))
				throw new ArgumentException("No value provided for coordinate string.", nameof(kmlString));

			var finalString = kmlString.Trim();

			var match = Regex.Match(finalString, KmlCoordPattern);
			if (!match.Success)
				throw new ArgumentException("Invalid format of coordinate string.", nameof(kmlString));

			var latS = match.Groups["lat"].Value;
			var lngS = match.Groups["lng"].Value;

			var latitude = Double.Parse(latS);
			var longitude = Double.Parse(lngS);

			return new Point(latitude, longitude);
		}

		public override string ToString()
		{
			// TODO: Fix format of leading zeros
			return $"{Latitude.ToString("000000")}N {Longitude.ToString("0000000")}E";
		}

		public static Point FromEAipCoords(double latitude, double longitude)
		{
			// https://epsg.io/4180
			//return new Point(latitude, longitude);
			//var fromProj = ProjectionInfo.FromProj4String("+proj=merc +lat_ts=0 +lon_0=0 +k=1.000000 +x_0=0 +y_0=0 +ellps=GRS80 +datum=WGS84 +units=m +no_defs no_defs"); // EPSG 42310
			//var fromProj = ProjectionInfo.FromProj4String("+proj=lcc +lon_0=24.0 +lat_0=57.51755393 +lat_1=58.0 +lat_2=59.3333333 +ellps=GRS80 +datum=WGS84 +units=degrees +no_defs"); // Custom eAIP
			var fromProj = ProjectionInfo.FromProj4String("+proj=lcc +lon_0=24 +lat_1=58 +lat_2=59.3333333 +lat_0=57.51755393 +lon_0=24 +x_0=0 +y_0=0 +ellps=GRS80 +datum=WGS84 +units=degrees"); // Custom eAIP
																																																  //var fromProj = ProjectionInfo.FromAuthorityCode("EPSG", 4935);
																																																  //var fromProj = ProjectionInfo.FromAuthorityCode("EPSG", 4180);
																																																  //var toProj = ProjectionInfo.FromAuthorityCode("EPSG", 3857); // EPSG 3857
			var toProj = ProjectionInfo.FromProj4String("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"); // Google maps
																															//var toProj = ProjectionInfo.FromProj4String("+proj=merc +lon_0=0 +lat_ts=0 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +units=m +no_defs "); // Google maps

			var longLat = new[] { longitude, latitude, };
			var z = new double[2];

			Reproject.ReprojectPoints(longLat, z, fromProj, toProj, 0, 1);

			return new Point(longLat[1], longLat[0]);
		}
	}
}