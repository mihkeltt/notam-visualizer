﻿namespace Web.Notam
{
	public class Circle : PointShape
	{
		public double? RadiusNM { get; set; }
	}
}