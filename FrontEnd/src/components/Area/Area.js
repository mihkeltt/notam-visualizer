import React from 'react';
import { hot } from 'react-hot-loader';
import { Polygon, Circle, Marker, InfoWindow } from "react-google-maps"

const path = require('path');

let GoogleMapsAPI = window.google.maps;

class Area extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showInfoWindow: false,
            //infoPosition: props.area.paths[0][0]
        };
    }

    handleClick(e){
        let text = "Designation: " + this.props.area.fullName + "\nType: " + this.props.area.type + "\nDescription: " + this.props.area.description;
        alert(text);
        var pos = e.latLng.toJSON();

        this.setState({
            showInfoWindow: true,
            //infoPosition: pos
        });
    }

    closeInfo(){
        this.setState({
            showInfoWindow: false
        });
    }

    render(){
        let fillOpacity = this.props.area.isWholeDay ? 0.3 : 0.15;
        let strokeOpacity = fillOpacity * 1.5;
        let color = this.props.area.isWholeDay ? "#FF0000" : "#013EFB";

        if(this.props.area.shape == "polygon")
            return <Polygon
                        paths = {this.props.area.paths}
                        options = {{
                            strokeColor: color,
                            strokeOpacity: strokeOpacity,
                            strokeWeight: 2,
                            fillColor: color,
                            fillOpacity: fillOpacity,
                            geodesic: true
                        }}
                        onClick= {this.handleClick.bind(this)}>
                        <InfoWindow onCloseClick={this.closeInfo.bind(this)} position={this.state.infoPosition}>
                            <div>
                                {this.props.area.description}
                            </div>
                        </InfoWindow>
                    </Polygon>

        if (this.props.area.shape == "circle")
        return <Circle
            center = {this.props.area.centerPoint}
            radius = {this.props.area.radiusMeters}
            options = {{
                strokeColor: color,
                strokeOpacity: strokeOpacity,
                strokeWeight: 2,
                fillColor: color,
                fillOpacity: fillOpacity,
                geodesic: true
            }}
            onClick = {this.handleClick.bind(this)}>
        </Circle>;

        if (this.props.area.shape == "point")
            return <Marker
                position = {this.props.area.centerPoint}
                icon = {path.resolve(__dirname, '../../assets/img/pin_marker_red.png')}
                onClick = {this.handleClick.bind(this)}>
            </Marker>

        return null;
    }
}

export default hot(module)(Area);