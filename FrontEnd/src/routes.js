import React from 'react';
import { Route } from 'react-router';

import { MainLayout } from './views/layouts';

import { MapPage, InfoPage } from './views/pages';

export default (
  <MainLayout>
    <Route exact path="/" component={MapPage} />
    <Route path="/info/:page" component={InfoPage} />
  </MainLayout>
);
