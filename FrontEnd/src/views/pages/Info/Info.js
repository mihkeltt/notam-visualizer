import React from 'react';
import { hot } from 'react-hot-loader';
import { Container, Row, Col } from 'reactstrap';

const InfoPage = (props) => (
  <Container>
    <Row>
      <Col>
        <div>
          <h2>Useful links</h2>
          <ul>
            <li>
              <a href="https://lennuluba.ee/">Lennuluba.ee</a>
            </li>
            <li>
              <a href="https://aim.eans.ee/">AIM</a>
            </li>
          </ul>
        </div>
      </Col>
    </Row>
  </Container>
);

export default hot(module)(InfoPage);
