import React from 'react';
import { hot } from 'react-hot-loader';
import { Container, Row, Col } from 'reactstrap';
import GoogleMapReact from 'google-map-react';
import { withGoogleMap, GoogleMap } from "react-google-maps"
import DatePicker from 'react-date-picker';
import Area from '../../../components/Area/Area'

let GoogleMapsAPI = window.google.maps;
let Enumerable = require("../../../../node_modules/linq")

class Map extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      from: null,
      to: null,
      areas: []
    };
  }

  componentDidMount() {
    this.loadMapData();
  }

  loadMapData(){
    var from = this.state.from;
    var to = this.state.to;

    if (to != null)
      to.setSeconds(86399);

    fetch("/api/notam/getareas", {
      method: "POST",
      body: JSON.stringify({
        fromUtc: from,
        toUtc: to
      }),
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      }
    })
      .then((response) => (response.json()))
      .then((areasData) => {
        var jsAreas = [];
        for (let area of areasData.areas) {
          if(!area.location)
            continue;

          var jsArea = {
            isOpen: true,
            id: area.id,
            name: area.name,
            description: area.description,
            fullName: area.fullName,
            fullDescription: area.fullDescription,
            upper: area.upper,
            lower: area.lower,
            isWholeDay: area.isWholeDay,
            type: area.type
          };

          if (area.location.points && area.location.points.length) {
            let path = Enumerable.from(area.location.points)
                .select((x) => (new GoogleMapsAPI.LatLng(x.latitude, x.longitude )))
                .toArray();

            let excludePath = area.excludeLocation != null && area.excludeLocation.points.length
            ? Enumerable.from(area.excludeLocation.points)
                .select((x) => (new GoogleMapsAPI.LatLng(x.latitude, x.longitude)))
                .reverse()
                .toArray()
            : null;

            jsArea.paths = excludePath == null ? [path] : [path, excludePath];
            jsArea.shape = "polygon";
          }

          var cp = area.location.centerPoint;
          if (cp){
            jsArea.centerPoint = new GoogleMapsAPI.LatLng(cp.latitude, cp.longitude)

            if(area.location.radiusNM)
            {
              jsArea.radiusMeters = area.location.radiusNM * 1852;
              jsArea.shape = "circle";
            }
            else{
              jsArea.shape = "point";
            }
          }

          jsAreas.push(jsArea);
        }

        this.setState({ areas: jsAreas });
      });
  }

  onFromDateChange(newVal){
    this.setState({from: newVal}, this.loadMapData);
  }

  onToDateChange(newVal){
    this.setState({to: newVal}, this.loadMapData);
  }

  today(){
    var from = new Date();
    from.setHours(0,0,0,0);

    var to = new Date();
    to.setHours(23, 59, 59, 0);

    this.setState({from: from, to: to}, this.loadMapData);
  }

  clearDates(){
    this.setState({from: null, to: null}, this.loadMapData);
  }

  renderMap(){
    const GoogleMapWrapper = withGoogleMap(props => (
      <GoogleMap
        defaultCenter={new GoogleMapsAPI.LatLng(58.7182, 25.0938)}
        defaultZoom={7}
        >
        {props.areas.map((area, i) => (
          <Area area={area} key={i} />
        ))}
      </GoogleMap>
    ));

    return (<GoogleMapWrapper
      areas = {this.state.areas}
      containerElement={
        <div style={{ height: '100%' }} />
      }
      mapElement={
        <div style={{ height: '100%' }} />
      } />);
  }

  render() {
    return <Container>
        <Row>
          <Col>
            <div className="map-container">
              { this.renderMap()}
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm={{ size: 8, offset: 2 }}>
            <DatePicker
              onChange={this.onFromDateChange.bind(this)}
              value={this.state.from} />
            <DatePicker
              onChange={this.onToDateChange.bind(this)}
              value={this.state.to} />
            <button onClick={this.today.bind(this)}>
              Today
            </button>
            <button onClick={this.clearDates.bind(this)}>
              All
            </button>
          </Col>
        </Row>
        <Row>
          <Col sm={{ size: 4, offset: 4 }}>
            <p>Work in progress. Use at your own risk!</p>
            <p>Author: Mihkel Tiidus</p>
            <p>
              <h3>Legend</h3>
              <ul>
                <li>Red - whole day restriction.</li>
                <li>Blue - scheduled restriction</li>
              </ul>
            </p>
            <p>
              <h3>Issues</h3>
              <ul>
                <li>"Today" selection is buggy.</li>
                <li>Aerodromes are missing schedules.</li>
              </ul>
            </p>
          </Col>
        </Row>
      </Container>;
  }
}

export default hot(module)(Map);