import React from 'react';
import { hot } from 'react-hot-loader';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
  Row,
  Col } from 'reactstrap';

class MainLayout extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      isOpen: true
    };
  }

  toggle(){
    console.log("Navbar toggled");
  }

  render(){
    return <div>
      <Navbar color="light" light expand="md">
          <NavbarBrand href="/">EE NOTAM visualizer</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Link to="/">Map</Link>
              </NavItem>
              <NavItem>
                <Link to="/info/stuff">Info</Link>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
    <hr />

    <main>
      {this.props.children}
    </main>
  </div>;
  }
}

export default hot(module)(MainLayout);
