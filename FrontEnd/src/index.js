import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import { rootEpic, rootReducer } from './modules/root';
import routes from './routes';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.scss';

const history = createHistory();
const middleware = applyMiddleware(routerMiddleware(history));
const store = createStore(rootReducer, middleware);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      {routes}
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app'),
);
