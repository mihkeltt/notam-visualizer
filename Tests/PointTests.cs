﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Web.Notam;

namespace Tests
{
	[TestClass]
	public class PointTests
	{
		[TestMethod]
		public void FromNEString_ProperInput_Success()
		{
			var input = "591728N 0273336E";

			var result = Point.FromNEString(input);

			Assert.AreEqual(59.1728m, result.Latitude);
			Assert.AreEqual(27.3336m, result.Longitude);
		}
	}
}
