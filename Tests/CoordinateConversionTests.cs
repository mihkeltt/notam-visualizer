﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DotSpatial.Projections;
using DotSpatial.Projections.AuthorityCodes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Web.Notam;

namespace Tests
{
	[TestClass]
	public class CoordinateConversionTests
	{
		[TestMethod]
		public void ConvertFromTo()
		{
			var fromProj = ProjectionInfo.FromProj4String("+proj=lcc +lon_0=24 +ellps=GRS80 +datum=WGS84");
			var toProj = ProjectionInfo.FromAuthorityCode("EPSG", 3857);

			var points = ((Polygon)AeronauticalAreas.PredefinedAreas.Last().Location).Points;
			var longLat = points
				.SelectMany(p => new[] { (double)p.Latitude, (double)p.Longitude })
				.ToArray();
			var z = new double[points.Count];

			Reproject.ReprojectPoints(longLat, z, fromProj, toProj, 0, points.Count);

			for (int i = 0; i < points.Count; i++)
			{
				Console.WriteLine($"{longLat[i].ToString("##.0000")} {longLat[i + 1].ToString("##.0000")}");
			}
		}

		[TestMethod]
		public void Authorities()
		{
			var auth = AuthorityCodeHandler.Instance;
		}

		[TestMethod]
		public void TestBestMatch()
		{
			var eaipLat = 59.322700178022586d;
			var eaipLng = 24.335105532491866d;
			var correctLat = 59.54083297181779d;
			var correctLng = 24.56416998134765d;
			var instance = AuthorityCodeHandler.Instance;
			var authType = typeof(AuthorityCodeHandler);
			var authField = authType.GetField("_authorityCodeToProjectionInfo", BindingFlags.Instance | BindingFlags.GetField | BindingFlags.NonPublic);
			var authDict = authField.GetValue(instance) as IDictionary<string, ProjectionInfo>;

			foreach (var sourceProjName in authDict.Keys)
			{
				var epsgCode = Int32.Parse(sourceProjName.Split(':')[1]);
				var fromProj = ProjectionInfo.FromEpsgCode(epsgCode);
				var toProj = ProjectionInfo.FromAuthorityCode("EPSG", 3857);

				var longLat = new[] {eaipLat, eaipLng};
				var z = new double[1];

				Reproject.ReprojectPoints(longLat, z, fromProj, toProj, 0, 1);

				if (Math.Abs(correctLat - longLat[0]) < 0.1 && Math.Abs(correctLng - longLat[1]) < 0.1)
				{
					Console.WriteLine(sourceProjName);
				}
			}
		}
	}
}