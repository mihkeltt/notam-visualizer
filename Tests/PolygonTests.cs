﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Web.Notam;

namespace Tests
{
	[TestClass]
	public class PolygonTests
	{
		[TestMethod]
		public void X()
		{
			var polygon = Polygon.FromPointsString("583200N 0261203E - 583026N 0261146E - 583024N 0261400E - 583156N 0261605E - 583200N 0261203E");

			Console.WriteLine(polygon);
		}
	}
}